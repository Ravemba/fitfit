/* eslint-disable object-shorthand */
const users = ['alina', 'yurii', 'andrii'];

module.exports = {
  authHandler: (req, res, next) => {
    if (users.includes(req.header('userId'))) {
      return next();
    }
    return res.status(401).end();
  }
};