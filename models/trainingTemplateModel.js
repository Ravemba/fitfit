/* eslint-disable linebreak-style */
/* eslint-disable comma-dangle */
/* eslint-disable linebreak-style */
const mongoose = require('mongoose');

const { Schema } = mongoose;

const trainingTemplateModel = new Schema(
  {
    title: { type: String },
    exersises: {
      type: Object,
      id: { type: String },
      title: { type: String },
      approaches: {
        type: Object,
        order: { type: Number },
        weight: { type: Number },
        count: { type: Number },
        timeout: { type: Number }
      },
    }
  }
);

module.exports = mongoose.model('Training Template', trainingTemplateModel);
