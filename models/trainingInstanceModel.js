const mongoose = require('mongoose');

const { Schema } = mongoose;

const trainingInstanceModel = new Schema(
  {
    title: { type: String },
    exersises: {
      type: Object,
      id: { type: String },
      title: { type: String },
      status: { type: String },
      createdAt: { type: Date },
      finishedAt: { type: Date },
      approaches: {
        type: Object,
        order: { type: Number },
        recommendedWeight: { type: Number },
        recomendedTimeout: { type: String },
        actualCount: { type: Number },
        actualWeight: { type: Number },
        actualTimeout: { type: String },
        status: { type: String },
        createdAt: { type: Date },
        finishedAt: { type: Date },
      },
    }
  }
);

module.exports = mongoose.model('Training Instance', trainingInstanceModel);
