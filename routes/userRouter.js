const express = require('express');
const usersController = require('../controllers/usersController');

function routes(User) {
  const userRouter = express.Router();
  const controller = usersController(User);

  userRouter.route('/')
    .post(controller.createNewUser)
    .get(controller.getUsers);

  userRouter.route('/:userId')
    .get(controller.getUser)
    .put(controller.updateUser)
    .patch(controller.updateUserField)
    .delete(controller.deleteUser);

  return userRouter;
}

module.exports = routes;
