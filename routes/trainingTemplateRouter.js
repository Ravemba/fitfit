const express = require('express');
const templateController = require('../controllers/trainingTemplateController');

function routes(Template) {
  const templateRouter = express.Router();
  const controller = templateController(Template);

  templateRouter.route('/')
    .post(controller.createNewTemplate)
    .get(controller.getTemplates);

  templateRouter.route('/:templateId')
    .get(controller.getTemplate)
    .put(controller.updateTemplate)
    .patch(controller.updateTemplateField)
    .delete(controller.deleteTemplate);

  return templateRouter;
}

module.exports = routes;
