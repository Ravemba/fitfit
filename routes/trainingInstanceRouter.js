const express = require('express');
const instanceController = require('../controllers/trainingInstanceController');

function routes(Instance) {
  const instanceRouter = express.Router();
  const controller = instanceController(Instance);

  instanceRouter.route('/')
    .post(controller.createNewInstance)
    .get(controller.getInstances);

  instanceRouter.route('/:instanceId')
    .get(controller.getInstance)
    .put(controller.updateInstance)
    .patch(controller.updateInstanceField)
    .delete(controller.deleteInstance);

  return instanceRouter;
}

module.exports = routes;
