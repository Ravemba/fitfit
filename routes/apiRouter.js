/* eslint-disable global-require */
const express = require('express');

function routes() {
  const apiRouter = express.Router();

  const User = require('../models/userModel');
  const Template = require('../models/trainingTemplateModel');
  const Instance = require('../models/trainingInstanceModel');
  const userRouter = require('./userRouter')(User);
  const templateRouter = require('./trainingTemplateRouter')(Template);
  const instanceRouter = require('./trainingInstanceRouter')(Instance);

  apiRouter.use('/api/users', userRouter);
  apiRouter.use('/api/templates', templateRouter);
  apiRouter.use('/api/instances', instanceRouter);

  return apiRouter;
}

module.exports = routes;