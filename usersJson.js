db.users.insert([
  {
    firstName: 'Alina',
    lastName: 'Nynyk',
    height: '171',
    weight: '55',
  },
  {
    firstName: 'Catherine',
    lastName: 'Durgin',
    height: '170',
    weight: '105',
  },
  {
    firstName: 'Barrett',
    lastName: 'Gaston',
    height: '180',
    weight: '95.5',
  },
  {
    firstName: 'Willie',
    lastName: 'Harper',
    height: '187',
    weight: '80.5',
  },
]);