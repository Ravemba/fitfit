function instancesController() {
  function createNewInstance(req, res) {
    const instance = {
      id: 1,
      exersises: {
        id: 1,
        title: 'Test exercise 1',
        status: 'In process',
        createdAt: '1 January, 2020, 3:20:00 UTC',
        finishedAt: '1 January, 2020, 4:25:00 UTC',
        approaches: {
          order: 1,
          recommendedWeight: 2,
          recomendedTimeout: '3',
          actualCount: 4,
          actualWeight: 5,
          actualTimeout: '6',
          status: 'Done',
          createdAt: '1 January, 2020, 3:23:00 UTC',
          finishedAt: '1 January, 2020, 4:28:00 UTC'
        }
      }
    };
    const httpStatusCode = 201;
    res.status(httpStatusCode);
    res.json(instance);
  }

  function getInstances(req, res) {
    const instances = [
      {
        id: 1,
        exersises: {
          id: 1,
          title: 'Test exercise 1',
          status: 'In process',
          createdAt: '1 January, 2020, 3:20:00 UTC',
          finishedAt: '1 January, 2020, 4:25:00 UTC',
          approaches: {
            order: 1,
            recommendedWeight: 2,
            recomendedTimeout: '3',
            actualCount: 4,
            actualWeight: 5,
            actualTimeout: '6',
            status: 'Done',
            createdAt: '1 January, 2020, 3:23:00 UTC',
            finishedAt: '1 January, 2020, 4:28:00 UTC'
          }
        }
      },
      {
        id: 2,
        exersises: {
          id: 2,
          title: 'Test exercise 2',
          status: 'Not Started',
          createdAt: '1 January, 2020, 3:20:00 UTC',
          finishedAt: '1 January, 2020, 4:25:00 UTC',
          approaches: {
            order: 1,
            recommendedWeight: 2,
            recomendedTimeout: '3',
            actualCount: 4,
            actualWeight: 5,
            actualTimeout: '6',
            status: 'Not Started',
            createdAt: '1 January, 2020, 3:23:00 UTC',
            finishedAt: '1 January, 2020, 4:28:00 UTC'
          }
        }
      }
    ];
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(instances);
  }

  function getInstance(req, res) {
    const instance = {
      id: 1,
      exersises: {
        id: 1,
        title: 'Test exercise 1',
        status: 'In process',
        createdAt: '1 January, 2020, 3:20:00 UTC',
        finishedAt: '1 January, 2020, 4:25:00 UTC',
        approaches: {
          order: 1,
          recommendedWeight: 2,
          recomendedTimeout: '3',
          actualCount: 4,
          actualWeight: 5,
          actualTimeout: '6',
          status: 'Done',
          createdAt: '1 January, 2020, 3:23:00 UTC',
          finishedAt: '1 January, 2020, 4:28:00 UTC'
        }
      }
    };
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(instance);
  }

  function updateInstance(req, res) {
    const instance = {
      id: 1,
      exersises: {
        id: 1,
        title: 'Test exercise Update',
        status: 'In process',
        createdAt: '1 January, 2020, 3:20:00 UTC',
        finishedAt: '1 January, 2020, 4:25:00 UTC',
        approaches: {
          order: 1,
          recommendedWeight: 2,
          recomendedTimeout: '3',
          actualCount: 4,
          actualWeight: 5,
          actualTimeout: '6',
          status: 'Done',
          createdAt: '1 January, 2020, 3:23:00 UTC',
          finishedAt: '1 January, 2020, 4:28:00 UTC'
        }
      }
    };
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(instance);
  }

  function updateInstanceField(req, res) {
    const instance = {
      id: 1,
      exersises: {
        id: 1,
        title: 'Test exercise Update field',
        status: 'In process',
        createdAt: '1 January, 2020, 3:20:00 UTC',
        finishedAt: '1 January, 2020, 4:25:00 UTC',
        approaches: {
          order: 1,
          recommendedWeight: 2,
          recomendedTimeout: '3',
          actualCount: 4,
          actualWeight: 5,
          actualTimeout: '6',
          status: 'Done',
          createdAt: '1 January, 2020, 3:23:00 UTC',
          finishedAt: '1 January, 2020, 4:28:00 UTC'
        }
      }
    };
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(instance);
  }

  function deleteInstance(req, res) {
    res.sendStatus(204);
  }

  return {
    createNewInstance,
    getInstances,
    getInstance,
    updateInstance,
    updateInstanceField,
    deleteInstance
  };
}

module.exports = instancesController;