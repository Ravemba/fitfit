function templatesController() {
  function createNewTemplate(req, res) {
    const template = {
      id: 1,
      title: 'Test 1',
      exersises: {
        id: 1,
        title: 'Test exercise 1',
        approaches: {
          order: 1,
          weight: 2,
          count: 3,
          timeout: 4
        }
      }
    };
    const httpStatusCode = 201;
    res.status(httpStatusCode);
    res.json(template);
  }

  function getTemplates(req, res) {
    const templates = [
      {
        id: 1,
        title: 'Test 1',
        exersises: {
          id: 1,
          title: 'Test exercise 1',
          approaches: {
            order: 1,
            weight: 2,
            count: 3,
            timeout: 4
          }
        }
      },
      {
        id: 2,
        title: 'Test 2',
        exersises: {
          id: 2,
          title: 'Test exercise 2',
          approaches: {
            order: 2,
            weight: 10,
            count: 4,
            timeout: 10
          }
        }
      }
    ];
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(templates);
  }

  function getTemplate(req, res) {
    const template = {
      id: 1,
      title: 'Test 1',
      exersises: {
        id: 1,
        title: 'Test exercise 1',
        approaches: {
          order: 1,
          weight: 2,
          count: 3,
          timeout: 4
        }
      }
    };
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(template);
  }

  function updateTemplate(req, res) {
    const template = {
      id: 1,
      title: 'Test Update',
      exersises: {
        id: 1,
        title: 'Test exercise Update',
        approaches: {
          order: 1,
          weight: 2,
          count: 3,
          timeout: 4
        }
      }
    };
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(template);
  }

  function updateTemplateField(req, res) {
    const template = {
      id: 1,
      title: 'Test Update Field',
      exersises: {
        id: 1,
        title: 'Test exercise Update Field',
        approaches: {
          order: 100,
          weight: 20,
          count: 3,
          timeout: 4
        }
      }
    };
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(template);
  }

  function deleteTemplate(req, res) {
    res.sendStatus(204);
  }

  return {
    createNewTemplate,
    getTemplates,
    getTemplate,
    updateTemplate,
    updateTemplateField,
    deleteTemplate
  };
}

module.exports = templatesController;