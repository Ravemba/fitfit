function usersController() {
  function createNewUser(req, res) {
    const user = {
      id: 1,
      firstName: 'Catherine',
      lastName: 'Durgin',
      height: '170',
      weight: '105'
    };
    const httpStatusCode = 201;
    res.status(httpStatusCode);
    res.json(user);
  }

  function getUsers(req, res) {
    const users = [
      {
        id: 1,
        firstName: 'Catherine',
        lastName: 'Durgin',
        height: '170',
        weight: '105'
      },
      {
        id: 2,
        firstName: 'Barrett',
        lastName: 'Gaston',
        height: '180',
        weight: '95.5'
      }
    ];
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(users);
  }

  function getUser(req, res) {
    const user = {
      id: 1,
      firstName: 'Catherine',
      lastName: 'Durgin',
      height: '170',
      weight: '105'
    };
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(user);
  }

  function updateUser(req, res) {
    const user = {
      id: 1,
      firstName: 'Catherine',
      lastName: 'PUT',
      height: '170',
      weight: '105'
    };
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(user);
  }

  function updateUserField(req, res) {
    const user = {
      id: 2,
      firstName: 'PATCH',
      lastName: 'PATCH',
      height: '200',
      weight: '2000'
    };
    const httpStatusCode = 200;
    res.status(httpStatusCode);
    res.json(user);
  }

  function deleteUser(req, res) {
    res.sendStatus(204);
  }

  return {
    createNewUser,
    getUsers,
    getUser,
    updateUser,
    updateUserField,
    deleteUser
  };
}

module.exports = usersController;