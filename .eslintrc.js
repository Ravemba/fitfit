module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    mocha: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    'comma-dangle': 0,
    'linebreak-style': 0,
    'eol-last': 0,
    'no-underscore-dangle': 0,
    'no-console': 0,
    'space-before-function-paren': 0,
    'max-len': 0
  }
};
