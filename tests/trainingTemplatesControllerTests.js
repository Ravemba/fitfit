/* eslint-disable no-unused-vars */
process.env.ENV = 'Test';

const sinon = require('sinon');
const assert = require('assert');
const chai = require('chai');

chai.use(require('sinon-chai'));

const templatesController = require('../controllers/trainingTemplateController');
const app = require('../app.js');

describe('trainingTemplateController', () => {
  const res = {
    status: () => {},
    sendStatus: () => {},
    json: () => {}
  };
  const status = sinon.spy(res, 'status');
  const sendStatus = sinon.spy(res, 'sendStatus');
  const json = sinon.spy(res, 'json');

  it('should call status() and json() as a response object of getTemplate method and pass status code and template as an argument', () => {
    templatesController().getTemplate(null, res);

    const template = {
      id: 1,
      title: 'Test 1',
      exersises: {
        id: 1,
        title: 'Test exercise 1',
        approaches: {
          order: 1,
          weight: 2,
          count: 3,
          timeout: 4
        }
      }
    };

    assert(json.withArgs(template).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call status() and json() as a response object of getTemplates method and pass status code and template as an argument', () => {
    templatesController().getTemplates(null, res);

    const templates = [
      {
        id: 1,
        title: 'Test 1',
        exersises: {
          id: 1,
          title: 'Test exercise 1',
          approaches: {
            order: 1,
            weight: 2,
            count: 3,
            timeout: 4
          }
        }
      },
      {
        id: 2,
        title: 'Test 2',
        exersises: {
          id: 2,
          title: 'Test exercise 2',
          approaches: {
            order: 2,
            weight: 10,
            count: 4,
            timeout: 10
          }
        }
      }
    ];

    assert(json.withArgs(templates).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call status() and json() as a response object of createNewTemplate method and pass status code and template as an argument', () => {
    templatesController().createNewTemplate(null, res);

    const template = {
      id: 1,
      title: 'Test 1',
      exersises: {
        id: 1,
        title: 'Test exercise 1',
        approaches: {
          order: 1,
          weight: 2,
          count: 3,
          timeout: 4
        }
      }
    };

    assert(json.withArgs(template).called, 'Wrong response body');
    assert(status.withArgs(201).calledOnce, 'Wrong status code');
  });

  it('should call status() and json() as a response object of updateTemplate method and pass status code and template as an argument', () => {
    templatesController().updateTemplate(null, res);

    const template = {
      id: 1,
      title: 'Test Update',
      exersises: {
        id: 1,
        title: 'Test exercise Update',
        approaches: {
          order: 1,
          weight: 2,
          count: 3,
          timeout: 4
        }
      }
    };

    assert(json.withArgs(template).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call status() and json() as a response object of updateTemplateField method and pass status code and template as an argument', () => {
    templatesController().updateTemplateField(null, res);

    const template = {
      id: 1,
      title: 'Test Update Field',
      exersises: {
        id: 1,
        title: 'Test exercise Update Field',
        approaches: {
          order: 100,
          weight: 20,
          count: 3,
          timeout: 4
        }
      }
    };

    assert(json.withArgs(template).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call sendStatus() as a response object of deleteTemplate method and pass status code as an argument', () => {
    templatesController().deleteTemplate(null, res);
    assert(sendStatus.withArgs(204).calledOnce, 'Wrong status code');
  });
});
