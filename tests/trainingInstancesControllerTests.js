/* eslint-disable no-unused-vars */
process.env.ENV = 'Test';

const sinon = require('sinon');
const assert = require('assert');
const chai = require('chai');

chai.use(require('sinon-chai'));

const instancesController = require('../controllers/trainingInstanceController');
const app = require('../app.js');

describe('trainingInstanceController', () => {
  const res = {
    status: () => {},
    sendStatus: () => {},
    json: () => {}
  };
  const status = sinon.spy(res, 'status');
  const sendStatus = sinon.spy(res, 'sendStatus');
  const json = sinon.spy(res, 'json');

  it('should call status() and json() as a response object of getInstance method and pass status code and instance as an argument', () => {
    instancesController().getInstance(null, res);

    const instance = {
      id: 1,
      exersises: {
        id: 1,
        title: 'Test exercise 1',
        status: 'In process',
        createdAt: '1 January, 2020, 3:20:00 UTC',
        finishedAt: '1 January, 2020, 4:25:00 UTC',
        approaches: {
          order: 1,
          recommendedWeight: 2,
          recomendedTimeout: '3',
          actualCount: 4,
          actualWeight: 5,
          actualTimeout: '6',
          status: 'Done',
          createdAt: '1 January, 2020, 3:23:00 UTC',
          finishedAt: '1 January, 2020, 4:28:00 UTC'
        }
      }
    };

    assert(json.withArgs(instance).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call status() and json() as a response object of getInstances method and pass status code and instance as an argument', () => {
    instancesController().getInstances(null, res);

    const instances = [
      {
        id: 1,
        exersises: {
          id: 1,
          title: 'Test exercise 1',
          status: 'In process',
          createdAt: '1 January, 2020, 3:20:00 UTC',
          finishedAt: '1 January, 2020, 4:25:00 UTC',
          approaches: {
            order: 1,
            recommendedWeight: 2,
            recomendedTimeout: '3',
            actualCount: 4,
            actualWeight: 5,
            actualTimeout: '6',
            status: 'Done',
            createdAt: '1 January, 2020, 3:23:00 UTC',
            finishedAt: '1 January, 2020, 4:28:00 UTC'
          }
        }
      },
      {
        id: 2,
        exersises: {
          id: 2,
          title: 'Test exercise 2',
          status: 'Not Started',
          createdAt: '1 January, 2020, 3:20:00 UTC',
          finishedAt: '1 January, 2020, 4:25:00 UTC',
          approaches: {
            order: 1,
            recommendedWeight: 2,
            recomendedTimeout: '3',
            actualCount: 4,
            actualWeight: 5,
            actualTimeout: '6',
            status: 'Not Started',
            createdAt: '1 January, 2020, 3:23:00 UTC',
            finishedAt: '1 January, 2020, 4:28:00 UTC'
          }
        }
      }
    ];

    assert(json.withArgs(instances).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call status() and json() as a response object of createNewInstance method and pass status code and instance as an argument', () => {
    instancesController().createNewInstance(null, res);

    const instance = {
      id: 1,
      exersises: {
        id: 1,
        title: 'Test exercise 1',
        status: 'In process',
        createdAt: '1 January, 2020, 3:20:00 UTC',
        finishedAt: '1 January, 2020, 4:25:00 UTC',
        approaches: {
          order: 1,
          recommendedWeight: 2,
          recomendedTimeout: '3',
          actualCount: 4,
          actualWeight: 5,
          actualTimeout: '6',
          status: 'Done',
          createdAt: '1 January, 2020, 3:23:00 UTC',
          finishedAt: '1 January, 2020, 4:28:00 UTC'
        }
      }
    };

    assert(json.withArgs(instance).called, 'Wrong response body');
    assert(status.withArgs(201).calledOnce, 'Wrong status code');
  });

  it('should call status() and json() as a response object of updateInstance method and pass status code and instance as an argument', () => {
    instancesController().updateInstance(null, res);

    const instance = {
      id: 1,
      exersises: {
        id: 1,
        title: 'Test exercise Update',
        status: 'In process',
        createdAt: '1 January, 2020, 3:20:00 UTC',
        finishedAt: '1 January, 2020, 4:25:00 UTC',
        approaches: {
          order: 1,
          recommendedWeight: 2,
          recomendedTimeout: '3',
          actualCount: 4,
          actualWeight: 5,
          actualTimeout: '6',
          status: 'Done',
          createdAt: '1 January, 2020, 3:23:00 UTC',
          finishedAt: '1 January, 2020, 4:28:00 UTC'
        }
      }
    };

    assert(json.withArgs(instance).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call status() and json() as a response object of updateInstanceField method and pass status code and instance as an argument', () => {
    instancesController().updateInstanceField(null, res);

    const instance = {
      id: 1,
      exersises: {
        id: 1,
        title: 'Test exercise Update field',
        status: 'In process',
        createdAt: '1 January, 2020, 3:20:00 UTC',
        finishedAt: '1 January, 2020, 4:25:00 UTC',
        approaches: {
          order: 1,
          recommendedWeight: 2,
          recomendedTimeout: '3',
          actualCount: 4,
          actualWeight: 5,
          actualTimeout: '6',
          status: 'Done',
          createdAt: '1 January, 2020, 3:23:00 UTC',
          finishedAt: '1 January, 2020, 4:28:00 UTC'
        }
      }
    };

    assert(json.withArgs(instance).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call sendStatus() as a response object of deleteInstance method and pass status code as an argument', () => {
    instancesController().deleteInstance(null, res);
    assert(sendStatus.withArgs(204).calledOnce, 'Wrong status code');
  });
});
