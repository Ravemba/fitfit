/* eslint-disable no-unused-vars */
process.env.ENV = 'Test';

const sinon = require('sinon');
const assert = require('assert');
const chai = require('chai');

chai.use(require('sinon-chai'));

const usersController = require('../controllers/usersController');
const app = require('../app.js');

describe('usersController', () => {
  const res = {
    status: () => {},
    sendStatus: () => {},
    json: () => {}
  };
  const status = sinon.spy(res, 'status');
  const sendStatus = sinon.spy(res, 'sendStatus');
  const json = sinon.spy(res, 'json');

  it('should call status() and json() as a response object of getUser method and pass status code and user as an argument', () => {
    usersController().getUser(null, res);

    const user = {
      id: 1,
      firstName: 'Catherine',
      lastName: 'Durgin',
      height: '170',
      weight: '105'
    };

    assert(json.withArgs(user).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call status() and json() as a response object of getUsers method and pass status code and user as an argument', () => {
    usersController().getUsers(null, res);

    const users = [
      {
        id: 1,
        firstName: 'Catherine',
        lastName: 'Durgin',
        height: '170',
        weight: '105'
      },
      {
        id: 2,
        firstName: 'Barrett',
        lastName: 'Gaston',
        height: '180',
        weight: '95.5'
      }
    ];

    assert(json.withArgs(users).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call status() and json() as a response object of createNewUser method and pass status code and user as an argument', () => {
    usersController().createNewUser(null, res);

    const user = {
      id: 1,
      firstName: 'Catherine',
      lastName: 'Durgin',
      height: '170',
      weight: '105'
    };

    assert(json.withArgs(user).called, 'Wrong response body');
    assert(status.withArgs(201).calledOnce, 'Wrong status code');
  });

  it('should call status() and json() as a response object of updateUser method and pass status code and user as an argument', () => {
    usersController().updateUser(null, res);

    const user = {
      id: 1,
      firstName: 'Catherine',
      lastName: 'PUT',
      height: '170',
      weight: '105'
    };

    assert(json.withArgs(user).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call status() and json() as a response object of updateUserField method and pass status code and user as an argument', () => {
    usersController().updateUserField(null, res);

    const user = {
      id: 2,
      firstName: 'PATCH',
      lastName: 'PATCH',
      height: '200',
      weight: '2000'
    };

    assert(json.withArgs(user).called, 'Wrong response body');
    assert(status.withArgs(200).called, 'Wrong status code');
  });

  it('should call sendStatus() as a response object of deleteUser method and pass status code as an argument', () => {
    usersController().deleteUser(null, res);
    assert(sendStatus.withArgs(204).calledOnce, 'Wrong status code');
  });
});
