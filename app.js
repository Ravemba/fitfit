const express = require('express'); // modules import
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const app = express(); // create instance

if (process.env.ENV === 'Test') {
  console.log('This is a test');
  const db = mongoose.connect('mongodb://localhost/userAPI_Test', { useNewUrlParser: true, useUnifiedTopology: true });
} else {
  console.log('This is for real');
  const db = mongoose.connect('mongodb://localhost/userAPI', { useNewUrlParser: true, useUnifiedTopology: true });
}

const port = process.env.PORT || 3000; // get port from config or default port 3000
const middlewares = require('./middlewares');
const apiRouter = require('./routes/apiRouter')();

app.use(bodyParser.json());
app.use(middlewares.authHandler);

app.use('/', apiRouter); // requests starting with /api are processed by userRouter

app.use('/', (req, res) => { // when request send to the port - going to respond back with the title
  res.status(404).end('Invalid Router');
});

app.server = app.listen(port, () => { // listening of the port from nodemon config
  console.log(`Running on port ${port}`);
});

module.exports = app;
